<!--
 Gota is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Gota is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Gota. If not, see <https://www.gnu.org/licenses/>.
-->

# TODO

## HIGH

- Do not overwrite symbolic links found
- Log history of early commands.
- Git commit SHA as source to link.

## MEDIUM

- `.dutignore` support hash-like folder. eg: .config{foo,bar,meh,forevis}
- Rollback feature.

## LOW

- Read-only symbolic links.

- JSON output of last files linked operation, useful for rollback
- Back up follows structure of folder `.config/code/user/settings.init` to `$HOME/.backup/.config/code/user/settings.init`
- If there is more than one file on backup folder with same name, prepend current time

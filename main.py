#!/usr/bin/env python3

"""Dut -Yet another simple and opinionated dot files manager."""

# Dut -Yet another simple and opinionated dot files manager.
# Copyright (C) 2024 EAS Barbosa

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
from pathlib import Path

# ================================= FARM


class Farm:
    """Farming of files."""

    def __init__(self):
        """.."""
        self.ignoredones: list[str] = self.ignore_files_default()

    def ignore_files_default(self) -> list[str]:
        """List of files or folders to be ignored."""
        return list(dict.fromkeys(list([".git", ".dutignore"]) + self.dutignore_list()))

    def dutignore_list(self) -> list[str]:
        """List of ignored files/folders in .dutignore file."""
        dutignore_file: str = str(Path(args.root).joinpath(".dutignore"))

        if not Path(dutignore_file).exists():
            print(".dutignogre not found, just symlinking everything")

        with open(dutignore_file) as rawdata:
            data: list[str] = rawdata.readlines()

        return [item.strip() for item in data]

    def files_to_ignore(self) -> list[str]:
        """List of files/folders to be ignored in absolute path."""
        files: list[str] = self.ignore_files_default()

        return list(map(lambda x: str(Path(args.root).joinpath(x)), files))

    def ignore_this(self, file: str) -> bool:
        """File should be ignored?."""
        for item in self.files_to_ignore():
            if file.startswith(item):
                return True

        return False


# ================================= CORE


class Core:
    """Core functionalities."""

    def __init__(self):
        """.."""
        self.destination: Path = self.dest()

    def dest(self) -> Path:
        """Set final destination."""
        return Path(args.dest) if args.dest else Path.home().joinpath("Downloads")

    def swap_root(self, filepath: Path) -> str:
        """Swap root of current filepath with either HOME or DESTINATION."""
        root = str(Path(args.root))

        return str(filepath).replace(f"{root}/", f"{str(self.destination)}/")

    def make_directory(self, directory: Path):
        """Make directory."""
        if directory.exists():
            return

        directory.mkdir

    def make_link(self):
        """."""
        pass


# ================================= ACTIONS


class Actions:
    """All actions to run."""

    def __init__(self, root):
        """Initiate it."""
        self.root = root
        self.core = Core()

    def create(self):
        """Create dotfiles."""
        if args.verbose:
            print("-- Creating symbolic links --\n")

        walk_root(self.root, lambda dirname, target: self.do_create(dirname, target))

    def do_create(self, dirname: Path, target: Path):
        link_dir = Path(self.core.swap_root(dirname))
        link = Path(self.core.swap_root(target))

        link_dir.mkdir(parents=True, exist_ok=True)

        if link.exists() and not link.is_symlink():
            print(f"backing up {str(link)}")
            return  # TODO: backup file to $HOME/.config/dut/backups

        if link.is_symlink() and link.exists():
            return

        print(f"{target} -> {link}")
        link.symlink_to(target)


# ================================= MAIN


def walk_root(root: str, func):
    """Walkthrough root folder."""

    farm = Farm()

    for dirpath, dirnames, filenames in os.walk(root):
        dirpath = Path(dirpath)

        for filename in filenames:
            filename = dirpath.joinpath(filename)

            if farm.ignore_this(str(filename)):
                continue

            if args.verbose:
                print(f"\nfrom: {str(filename)}")

            func(dirpath, filename)

    exit(0)


# ================================= CLI

parser = argparse.ArgumentParser(
    prog="Dut", description="Yet another simple and opinionated dot files manager."
)
parser.add_argument(
    "-p", "--pretend", help="simulate the linking process", action="store_true"
)
parser.add_argument("-c", "--create", help="create dotfiles links", action="store_true")
parser.add_argument("-r", "--root", help="root folder with dotfiles")
parser.add_argument(
    "-d", "--dest", help="destination folder to mirrors root's dotfiles"
)
parser.add_argument(
    "-i", "--verbose", help="provide additional information", action="store_true"
)
parser.add_argument("-v", "--version", action="version", version="%(prog)s 0.1")
args = parser.parse_args()


def main():
    """Go for it."""
    try:
        if not args.root:
            print("error: root folder must be provided: --root, -r.\n")
            parser.print_help()
            exit(1)

        root = Path(args.root)
        actions = Actions(root)
        farm = Farm()

        if args.verbose:
            print(f"root: {root}")
            if args.dest:
                print(f"destination: {args.dest}")
            print(f"ignore: {farm.ignore_files_default()}")
            print("\n")

        if args.create:
            actions.create()

        print(
            "error: One of the actions are required: --create, --remove, --pretend, --overwrite."
        )
        exit(1)
    except KeyboardInterrupt:
        print("exiting!")
        exit(0)


if __name__ == "__main__":
    main()

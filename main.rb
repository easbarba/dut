#!/usr/bin/env ruby
# frozen_string_literal: true

# Dut -Yet another simple and opinionated dot files manager.
# Copyright (C) 2024 EAS Barbosa

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "pathname"
require "find"
require "optparse"
require "fileutils"

# list of filtered files/folders to link
class Farm
  HOME = Pathname.new Dir.home

  attr_reader :all

  def initialize(params)
    @from = Pathname.new(params[:from]).expand_path
    @destination = if params[:to].nil?
        HOME
      else
        Pathname.new(params[:to]).expand_path
      end
    @all = {}.tap { |f| all_items[:files].each { |t| f.store(t, destination_dir(t)) } }
  end

  # ignore these dotfiles
  def dotignored
    dots = @from.join(".dutignore").read.split "\n"
    dots.append ".dutignore" # ignore itself too, ofc!
    dots.uniq # users may not notice duplicates.
  end

  # is ITEM included in from folder?
  def ignore?(item)
    dotignored.map { |x| item.to_path.include? @from.join(x).to_path }.any?
  end

  # organize listed items in .dutignore as pathname
  def all_items
    { files: [], folders: [] }.tap do |x|
      Find.find(@from) do |item|
        item = Pathname.new item

        next if item == @from # skip the from folder itself
        next if ignore? item

        item.file? ? x[:files] << item : x[:folders] << item
      end
    end
  end

  # transform  stringfied origin item's from absolute path to home
  # /a/b/c.tar --> /home/b/c.tar
  def destination_dir(item)
    Pathname.new(item.to_path.gsub(@from.to_path, @destination.to_path))
  end
end

# Mirrors, by symlinking, a dotfiles repository to $HOME or selected folder.
class Core
  HOME = Pathname.new Dir.home

  def initialize(params)
    @params = params
  end

  # do not symlink but create top folders of files if it does not exist
  def make_folder(link)
    folder = link.dirname
    return if folder.exist?

    # return if link == HOME # do not create the $HOME folder :/

    puts "Creating folder: #{folder}"
    FileUtils.mkdir_p folder unless @params[:pretend]
  end

  # move file from home to a /home/backup/{file}
  def backup_item(link)
    return unless link.exist?
    return if link.symlink?

    warn "backup: #{link} ❯ $HOME/.backup."
    home_backup_folder = HOME.join(".backup")
    FileUtils.mkdir_p home_backup_folder
    FileUtils.mv link, home_backup_folder unless @params[:pretend]
  end

  # delete symlink if symlink's target does not exist
  def remove_faulty_link(link)
    return if link.exist?
    return unless link.symlink? # skip as link is a symlink and aint faulty

    warn "purging broken link: #{link}"
    link.delete unless @params[:pretend]
  end

  def link_file(target, link)
    # unless forced to, skip linking file as it does exist and is a symbolic link.
    return if link.symlink?

    puts "linking: #{target} ❯ #{link}"
    link.make_symlink target unless @params[:pretend]
  end

  def fix_perm(link)
    return if link.symlink?
    return if @params[:pretend]

    puts "updating permission of #{link}"
    link.chmod 0o744
  end
end

# All actions operations
class Actions
  def initialize(params)
    @params = params
    @farm = Farm.new(params).all
    @core = Core.new(params)
  end

  def wipe
    @farm.each do |_, link|
      link.delete if link.exist? && link.symlink?
    end
  end

  def pretend
    puts "-- pretend-mode --"
    create
  end

  def overwrite
    wipe
    create
  end

  def create
    @farm.each do |target, link| # As enumerator yielding folder to symlink
      @core.make_folder link
      @core.backup_item link
      @core.remove_faulty_link link
      @core.link_file target, link
      @core.fix_perm link
    end
  end

  def info
    puts <<~EOL
           ... General information ...

           from: #{@params[:from]}
           destination: #{@params[:to]}
         EOL

    exit
  end
end

params = {}
options = OptionParser.new do |parser|
  parser.banner = "Usage: dut [options]"

  parser.on("-f", "--root DIR", String, "folder with dotfiles")
  parser.on("-t", "--dest DIR", String, "location where to link files")
  parser.on("-c", "--create", "create dotfiles links")
  parser.on("-r", "--wipe", "remove created dotfiles links")
  parser.on("-o", "--overwrite", "force recreating of dotfiles links")
  parser.on("-p", "--pretend", "mimic creating of symbolic links")
  parser.on("-i", "--info", "general information of internals commands")
end

options.parse! ["--help"] if ARGV.empty?
options.parse!(into: params)

# RUN
actions = Actions.new(params)
actions.info if params[:info]
actions.wipe if params[:wipe]
actions.create if params[:create]
actions.pretend if params[:pretend]
actions.overwrite if params[:overwrite]

#!/usr/bin/env php
<?php

declare(strict_types=1);

/*
 * Dut -Yet another simple and opinionated dot files manager.
 * Copyright (C) 2024 EAS Barbosa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



# ======================================= MIDDLEWARE


# ======================================= ACTIONS

class Actions
{
    public function __construct(
        private string $root,
        private string|null $destination = null,
        private bool $verbose = false
    )
    {
        $this->destination = $destination ?? getenv('HOME');
    }

    public function create(): void
    {
        print("In the beginning, there was nothing! Let there be links!");
    }

    public function wipe(): void
    {
        print("No, no, no, no, no, no, Aren't you something");
    }

    public function pretend(): void
    {
        print("If you dont say, I wont say too, and no one will know!");
    }

    public function overwrite(): void
    {
        print("oh no, something is wrong, be a dear and overwrite it for me?!");
    }

    public function info(): void
    {
        $verbose = $this->verbose ? 'yes' : 'no';
        echo <<< INFO
            root: {$this->root}
            destination: {$this->destination}
            verbose: {$verbose}
        INFO;
    }
}

# ============================================= CLI


$shortopts = "";
$shortopts .= "d:";
$shortopts .= "r:";
$shortopts .= "i::";
$shortopts .= "cwpohv";

$longopts = [
    "dest:",
    "root:",
    "create",
    "wipe",
    "pretend",
    "overwrite",
    "verbose",
    "help",
    "info",
    "info::",
];

$options = getopt($shortopts, $longopts);

# ======================================= HELPERS


/**
 * @param array<int,string> $keys
 * @param array<string,bool> $options
*/
function option_get(array $keys, array $options): string|null
{
    if($keys === false) {
        return null;
    }

    foreach ($keys as $key) {
        if(array_key_exists(key: $key, array: $options))
        {
            if($options[$key] === false)
            {
                return null;
            }

            return $options[$key];
        }
    }

    return null;
}

function help_usage(): void
{
echo <<< HELP
Usage: dut [options]

  -d DIR, --dest DIR    destination folder to deliver links
  -r DIR, --root DIR    target folder with dotfiles
  -c, --create          create links of dotfiles
  -w, --wipe            remove links from target folder
  -p, --pretend         demonstrate files linking
  -o, --overwrite       overwrite existent links
  -i, --info            provide overall information found
  -v, --verbose         display additional information
  -h, --help            display this help\n
HELP;
}


if (option_get(['help', 'h'], $options)  !== null)
{
    help_usage();
    exit(0);
}

if (option_get(['root', 'r'], $options) === null)
{
    echo "Error: [--root,-r] is required!\n\n";
    help_usage();
    exit(1);
}

$actions = new Actions(root: option_get(['root', 'r'], $options),
                       destination: option_get(['destination', 'd'], $options),
                       verbose: option_get(['verbose', 'v'], $options) === null);

foreach ($options as $key => $value) {
    switch ($key) {
        case "c":
        case "create":
            $actions->create();
            break;
        case "w":
        case "wipe":
            $actions->wipe();
            break;
        case "p":
        case "pretend":
            $actions->pretend();
            break;
        case "o":
        case "overwrite":
            $actions->overwrite();
            break;
        case "i":
        case "info":
            $actions->info();
    }
}


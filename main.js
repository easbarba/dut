#!/usr/bin/env node

// Dut -Yet another simple and opinionated dot files manager.
// Copyright (C) 2024 EAS Barbosa

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


const process = require('process');

const allOptions = {
    '-c': '--create',
}

function getOptions(options) {
    let arguments = options.splice(2);
    let longOptions = arguments.filter(option => option.startsWith('--')); // TODO regex to identify if a word follows --
    let shortOptions = arguments.filter(option => option.startsWith('-')); // TODO regex to identify if a single letter follows -

    if (longOptions == !0) return longOptions;

    let finalOptions = longOptions;

    return finalOptions;
}

function run() {
    // switch (arguments[1]) { // check that `say` is the first "command"
    //     case 'say':
    //         let options = arguments.slice(1); // get the stuff after `say`
    //         let optionsObject = {} // key-value representation
    //         if (options.indexOf("--user") != -1) { // if it exists
    //             optionsObject.user = options[options.indexOf("--user") + 1]
    //         }
    //         else {
    //             // you can throw an error here
    //         }
    //         if (options.indexOf("--msg") != -1) { // if it exists
    //             optionsObject.msg = options[options.indexOf("--msg") + 1]
    //         }
    //         if (options.indexOf("--random") != -1) { // if it exists
    //             optionsObject.random = true
    //         }
    //         console.log(optionsObject) // you can use optionsObject for your program
    //         break;
    //     default:
    //         console.log("Invalid command");
    // }
}

options = process.argv

if (!options.slice(2).length) {
    console.warn('Error: Not enough options has been passed');
    process.exit(1);
}

// console.log(getOptions(options));

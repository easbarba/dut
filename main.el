:;exec emacs -batch -l "$0" -f main "$@"

;;; main.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 EAS Barbosa
;;
;; Author: EAS Barbosa <easbarba@outlook.com>
;; Maintainer: EAS Barbosa <easbarba@outlook.com>
;; Created: November 13, 2024
;; Modified: November 13, 2024
;; Version: 0.0.1
;; Keywords: command-line dotfiles manager linux bsd
;; Homepage: https://github.com/easbarba/dut
;; Package-Requires: ((emacs "24.3"))
;;

(defun main ()
  (print (version))
  (print (format "Options in %s" command-line-args-left)))

;; Local Variables:
;; mode: emacs-lisp
;; End:

(provide 'main)
;;; main.el ends here

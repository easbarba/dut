#!/usr/bin/awk -f

# Dut -Yet another simple and opinionated dot files manager.
# Copyright (C) 2024 EAS Barbosa
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

BEGIN {
    options["root"] = false
    options["target"] = false
    options["create"] = false
    options["pretented"] = false

    cli_options_parse()
	print (options)
}

function cli_options_parse()
{
	if (ARGC == 1) { print_usage() }

    for(i in ARGV) {
        if (ARGV[i] == "--help") { print_usage() }
        if (ARGV[i] == "--create") { options["create"] = true }
        if (ARGV[i] == "--pretend") { options["pretend"] = true }

        if (ARGV[i] == "--root") { print("roooting"); exit 0 }
        # if (ARGV[i] == "root") {
        #     if (ARGV[i+1]) {
        #     options["root"]
    }
        # print(ARGV[i])
    # }

    # options["root"] = ARGV[2]
    # print("root: ", ARGV[2])
    # print("options length: " length(options))
    exit 0
}

function print_usage()
{
	print "dut [options]\n\n\
  -d DIR, --dest DIR    destination folder to deliver links\n\
  -r DIR, --root DIR    target folder with dotfiles\n\
  -c, --create          create links of dotfiles\n\
  -w, --wipe            remove links from target folder\n\
  -p, --pretend         demonstrate files linking\n\
  -o, --overwrite       overwrite existent links\n\
  -i, --info            provide additional information\n\
  -v, --version         display version\n\
  -h, --help            display this help"

    exit 0
}

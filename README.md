<!--
 Dut is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Dut is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Dut. If not, see <https://www.gnu.org/licenses/>.
-->

# Dut

Yet another simple, and opinionated, dotfiles manager.

Files in ROOT directory are symbolic linked to DESTINE directory, no destructive operation is done. If there is a real file in DESTINE with same name of ROOT/file this will be backed up to a directory in $HOME with timestamp and preserving original directory structure.  

WARNING: Linux-based and UNIX are the only supported system.

## Usage

```sh
dut --root /data/dotfiles-repo --dest $HOME/Downloads --create
dut -r /data/bin --dest $HOME/.local/bin --overwrite --info
dut -r /data/bin --dest $HOME/.local/bin --wipe
dut -r /data/dotfiles-repo -d $HOME --pretend
dut --help
```

## Options

| Option          | Description                               |
|-----------------|-------------------------------------------|
| -h, --help      | show this help message and exit           |
| -p, --pretend   | simulate the linking process              |
| -c, --create    | create links from dotfiles                |
| -d, --dest DEST | directory destine to make links           |
| -r, --root ROOT | root directory with dotfiles              |
| -w, --wipe      | remove links created in destine directory |
| -o, --overwrite | overwrite existent links                  |
| -i, --info      | provide additional information            |
| -v, --version   | display version                           |

## Progress

| lang   | completion          |
|--------|---------------------|
| guile  | create,pretend,wipe |
| ruby   | create,pretend,wipe |
| golang | create,pretend      |
| python | create,pretend      |
| php    | initial             |
| bash   | initial             |

## [TODO](TODO.md)

## [Protocol](PROTOCOL.md)

## LICENSE

[GPL-v3](https://www.gnu.org/licenses/gpl-3.0.en.html)

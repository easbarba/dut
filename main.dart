#!/usr/bin/dart
// Dut -Yet another simple and opinionated dot files manager.
// Copyright (C) 2024 EAS Barbosa

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:io' show Directory, File, FileSystemEntity, Platform, exit;

List<String> ignoreDefaultList = [".git"];
String? home = Platform.environment['HOME'];
List<String> actionsList = ["-c", "-o", "-r", "-p"];
List<String> locationsList = ["-t", "-f"];
String bannerUsage =
    """dut - Yet another simple and opinionated dot files manager.

locations:
  -t DIR, --to DIR      destination folder to deliver links
  -f DIR, --from DIR    target folder with dotfiles

actions:
  -c, --create          create links of dotfiles
  -r, --remove          remove links from target folder
  -p, --pretend         demonstrate files linking
  -o, --overwrite       overwrite existent links
  -i, --info            provide additional information
  -v, --version         display version
  -h, --help            display this help""";

void main(List<String> args) {
  try {
    var locations = parseShort(args)[1];
    var actions = parseShort(args)[0];

    walkDir(
      locations['-f'].toString(),
      actions.keys.firstWhere((element) => true), // return first truthy action
    );
  } catch (exception) {
    print(exception);
    exit(1);
  }
}

// ACTIONS

// create links of dotfiles
void create(FileSystemEntity from, [String to = ""]) {
  if (from is Directory) {
    print("Directory: ${from.path}");
  }

  if (from is File) {
    print("File: ${from.path}");
  }
}

void overwrite() {
  throw "not implemented";
}

void remove() {
  throw "not implemented";
}

void pretend() {
  throw "not implemented";
}

void info() {
  throw "not implemented";
}

// MIDDLEWARE

// HELPERS

void walkDir(String directory, String action) async {
  var dir = Directory(directory);
  dir
      .list(recursive: true, followLinks: false)
      .where((ent) =>
          !(ent.path.startsWith(dir.path + "/" + ignoreDefaultList[0])))
      .forEach((final ent) {
    if (action.compareTo("-c") == 0) {
      create(ent);
    }
  });
}

// TODO: properly return
List<Map<String, Object>> parseShort(List<String> args) {
  var actions = Map<String, bool>();
  var locations = Map<String, String>();
  var argsMapped = args.asMap();

  argsMapped.forEach(
    (i, x) {
      if (x.startsWith('-')) {
        // check if provided action is listed.
        if (actionsList.contains(argsMapped[i]!)) {
          actions[x] = true;

          // check if provided location is listed.
        } else if (locationsList.contains(argsMapped[i]!)) {
          locations[x] = argsMapped[i + 1]!;
        }
      }
    },
  );

  if (locations.length == 0 && actions.length == 0) {
    throw bannerUsage;
  }

  if (locations.length == 0) {
    throw 'No locations provided, -f is required!';
  }

  if (actions.length == 0) {
    throw 'No actions provided, an action ($actionsList) is required!';
  }

  return [actions, locations];
}

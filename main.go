// Dut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Dut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// GLOBAL
var targetDir, destinationDir string
var pretendAction bool = false

func main() {
	actionsFlags, destFlag, rootFlag := cliOptions()
	targetDir = filepath.Clean(*rootFlag)      // remove trailing /
	destinationDir = filepath.Clean(*destFlag) // remove trailing /

	crawler(targetDir, ignoredFiles(), actionsFlags)
}

// ACTIONS

func info(ignored []string, root string) {
	println("")
	fmt.Println("Ignored: ", ignored)
	fmt.Println("Root: ", root)
	fmt.Println()
}

func create(filePath string) {
	doLinking(filePath)
}

func overwrite(filePath string) {
	panic("Not implemented")
}

func pretend(filePath string) {
	pretendAction = true
	doLinking(filePath)
}

func remove(file string) {
	panic("Not implemented")
}

// HELPERS

// exchange target directory to destination directory
func toDestination(filePath, destination string) string {
	return strings.Replace(filePath, targetDir, destination, 1)
}

// create all parents directory down to link name
func mkdirp(filePath, destination string) {
	rootfile, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer rootfile.Close()

	fileInfo, err := rootfile.Stat()
	if err != nil {
		panic(err)
	}

	toDestinationPath := toDestination(filePath, destination)
	if fileInfo.IsDir() {

		// if destination directory does not exist, create
		if _, err := os.Stat(toDestinationPath); err != nil {
			fmt.Println(toDestinationPath, "directory not found, creating!")

			err = os.MkdirAll(toDestinationPath, 0755)
			if err != nil {
				panic(err)
			}
		}
	}
}

func doLinking(filePath string) {
	fileInfo, err := os.Lstat(filePath)
	if err != nil {
		panic(err.Error())
	}

	if fileInfo.Mode()&os.ModeSymlink != 0 {
		backupFile(toDestination(filePath, destinationDir))
		return
	}

	if fileInfo.IsDir() {
		mkdirp(filePath, destinationDir)
		return
	}

	printLinking(filePath)

	if pretendAction {
		return
	}

	os.Symlink(filePath, toDestination(filePath, destinationDir))
}

func printLinking(filePath string) {
	fmt.Printf("\nfrom: %s\nto:   %s\n", filePath, toDestination(filePath, destinationDir))
}

// TODO: backup files
func backupFile(filePath string) {
	homeDir, _ := os.UserHomeDir()
	backupDir := path.Join(homeDir, "onur_backup")

	// mkdirp(filePath, toDestination(filePath, backupDir))
	fmt.Printf("backing up to: %s\n", toDestination(filePath, backupDir))
}

// INTERNALS

// get all files to be ignored
func ignoredFiles() []string {
	var ignoreFileName = ".dutignore"
	ignoreList, err := os.ReadFile(filepath.Join(targetDir, ignoreFileName))

	result := []string{".git", ".gitignore", ignoreFileName}

	// no .dutignore file found, return sensible default files to ignore
	if err != nil {
		return result
	}

	// sensible default + user list of files to ignore
	result = append(result, strings.Split(string(ignoreList), "\n")...)

	return result
}

// command line arguments parser
func cliOptions() (map[string]*bool, *string, *string) {
	createFlag := flag.Bool("create", false, "create links of dotfiles")
	removeFlag := flag.Bool("remove", false, "remove links from target directory")
	pretendFlag := flag.Bool("pretend", false, "demonstrate files linking")
	overwriteFlag := flag.Bool("overwrite", false, "overwrite existent links")
	infoFlag := flag.Bool("info", false, "provide additional information")

	actionsFlags := map[string]*bool{
		"create":    createFlag,
		"remove":    removeFlag,
		"pretend":   pretendFlag,
		"overwrite": overwriteFlag,
		"info":      infoFlag,
	}

	destFlag := flag.String("dest", "", "destination directory to deliver links")
	rootFlag := flag.String("root", "", "target directory with dotfiles")

	flag.Parse()

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "dut, yet another simple dotfiles manager. \n\n")
		fmt.Fprintln(flag.CommandLine.Output(), "Usage information:")
		flag.PrintDefaults()
	}

	if *rootFlag == "" {
		flag.Usage()
		os.Exit(0)
	}

	return actionsFlags, destFlag, rootFlag
}

// pick action to run
func actionPicker(actions map[string]*bool, filePath string) {
	switch {
	case *actions["create"]:
		create(filePath)
	case *actions["remove"]:
		remove(filePath)
	case *actions["pretend"]:
		pretend(filePath)
	case *actions["overwrite"]:
		overwrite(filePath)
	default:
		fmt.Println("No action selected!")
		os.Exit(1)
	}
}

func crawler(root string, ignored []string, actions map[string]*bool) {
	if *actions["info"] {
		info(ignored, root)
	}

	filepath.WalkDir(root, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println(err)
			return err
		}

		// ignore root directory
		if filePath == root {
			return nil
		}

		// ignore files listed in .dutignore
		if ignoreThis(filePath, ignored) {
			return nil
		}

		actionPicker(actions, filePath)

		return nil
	})
}

// ignores current file if either its absolutely path
// or its parent directory is listed in the .dutignored
func ignoreThis(filePath string, ignoreList []string) bool {
	_, fileWoRoot, _ := strings.Cut(filePath, targetDir+"/")
	// if !found {
	// 	return true
	// }

	for _, ignoreItem := range ignoreList {
		// ignore empty string
		if ignoreItem == "" {
			continue
		}

		if r := strings.HasPrefix(fileWoRoot, ignoreItem); r {
			return true
		}
	}

	return false
}

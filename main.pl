#!/usr/bin/perl -w

# Dut -Yet another simple and opinionated dot files manager.
# Copyright (C) 2024 EAS Barbosa

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


use v5.32;
use utf8;
use warnings;
use strict;
use Getopt::Long 'HelpMessage';
use Time::Piece;
use File::Find;

# CLI PARSING

GetOptions(
    'from=s' => \(my $from_name = ''),
    'to=s' => \(my $to_name = '/home/'),
    'create' => sub { create() },
    'remove' => \(my $remove = 0),
    'pretend' => \(my $pretend = 0),
    'overwrite' => \(my $overwrite = 0),
    'info' =>  sub { say info() },
    'version' =>  sub { say "0.0.1" },
    'help' => sub { HelpMessage(0) }
) or HelpMessage(1);

die HelpMessage unless $from_name;

# die unless we got the mandatory argument
HelpMessage(1) unless $from_name;

sub info
{
    return "-- general information --
from: $from_name
to: $to_name"
}

# tbc
sub print_license { ... }

=head1 NAME

license - get license texts at the command line!

=head1 SYNOPSIS

  --from,-f        target folder with dotfiles
  --to,-to         destination folder to deliver links
  --create,-c      create links of dotfiles
  --remove,-r      remove links from target folder
  --pretend,-p     demonstrate files linking
  --overwrite,-o   overwrite existent links
  --info,-i        provide additional information
  --help,-h        display help usage
  --version,-v     display version

=head1 VERSION

0.01

=cut


# MIDDLEWARE
sub walk {
  find(\&wanted, $from_name);
}

sub wanted {
    print "$File::Find::name\n ";
}

# ACTIONS
sub create {
  walk
}

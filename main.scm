#!/usr/bin/guile \
--no-auto-compile -e main -s
!#

;; Dut -Yet another simple and opinionated dot files manager.
;; Copyright (C) 2025 EAS Barbosa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; TODO:
;; - Logging actions
;; - roll-back
;; - improve backup system

(import (rnrs base)
        (rnrs io ports)
        (rnrs io simple)
        (ice-9 getopt-long)
        (ice-9 ftw)
        (ice-9 format))

;; ========================================== GLOBAL

(define homedir (getenv "HOME"))
(define version "0.1.0")

;; ========================================== .dutignored

(define dutignored-name ".dutignore")

;; ignore file is provided by user?
(define (dutignore-exist? filepath)
  (file-exists? filepath))

;; ROOT ignored file absolute path
(define (dutignore-path root)
  (string-append root "/" dutignored-name))

;; return all listed files to be ignored in .dutignore
(define (ignored-files-found root)
  (string-split (call-with-input-file (dutignore-path root) get-string-all)
                #\newline))

(define (ignored-files root)
  (let ([files (ignored-files-found root)])
    (map (lambda (i) (if (not (member i files))
                         (cons i files)))
         files)
    (cons dutignored-name
          (cons ".git" files))))

;; ========================================== MIDDLEWARE

;; Directory residing all dotfiles to link.
(define (root-dir options)
  (canonicalize-path (cdr (assv 'root options))))

;; Destination directory, defaults to $HOME.
(define (destine-dir options)
  (let ([dest (if (assq 'dest options)
                  (cdr (assv 'dest options))
                  homedir)])

    ;; create DEST if it does not exist yet.
    (unless (file-exists? dest)
      (mkdir dest))

    (canonicalize-path dest)))

;; return: string
;; remove rootdir from current filename "/rootdir/filename" -> "filename"
(define (rootdir-remove filename rootdir)
  (let* ([rootdir-length (if (string-ci= rootdir filename)
                             (string-length rootdir) ;; if filename is rootdir return without heading /
                             (+ 1 (string-length rootdir)))])
    (string-replace filename "" 0 rootdir-length)))

;; returns DESTINE/.config/meh/FILENAME to $HOME/.config/meh/FILENAME
(define (root-to-home filename)
  (string-append homedir "/" filename))

;; is FILENAME listed in .dutignore?
(define (root-ignore? filepath rootdir) ;; FIX: not ignore .git directory
  (member #t (map (lambda (ignore-file)
                    (unless (string-null? ignore-file)
                      ;; (newline) (display (format #f "i: ~a -> f: ~a ? ~a n: ~a" ignore-file filepath (string-prefix? ignore-file filepath) (string-null? ignore-file)))
                      (string-prefix? ignore-file filepath)))
                  (ignored-files rootdir))))

;; walk recursively through the ROOTDIR directory and run ACTION.
(define (walk rootdir action)
  (ftw rootdir
       (lambda (current-filename statinfo flag)
         (let* ([filepath (rootdir-remove current-filename rootdir)])
           (unless (root-ignore? filepath rootdir)
             (action current-filename filepath))
           #t))))


;; ========================================== ACTIONS

(define (create options)
  (walk (root-dir options)
        (lambda (root-dir linkpath)
          (linking root-dir
                   (linkpath-destine-dir (destine-dir options) linkpath)))))

(define (wipe options)
  (walk (root-dir options)
        (lambda (root-dir linkpath)
          (let ([linkpath-destined (linkpath-destine-dir (destine-dir options) linkpath)])
            (unless (file-is-directory? linkpath-destined)
              (display (format #f "\n-l: ~a" linkpath-destined))
              (delete-file linkpath-destined))))))

(define (pretend options)
  (display "-> Initiate dry-run mode\n")
  (walk (root-dir options)
        (lambda (root-dir linkpath)
          (linking root-dir
                   (linkpath-destine-dir (destine-dir options) linkpath)
                   #t)))
  (display "\n-> End dry-run mode"))

(define (overwrite options)
  (display 'overwriting))

(define (info options)
  (let ([rootdir (root-dir options)])
    (display (format #f "rootdir: ~a\ndestination: ~a\nignore: ~a"
                     rootdir
                     (destine-dir options)
                     (string-join (ignored-files rootdir) " ")))
    (exit 0)))

;; ========================================== HELPERS

;; link file or create directory.
(define* (linking root-dir linkpath #:optional pretend)
  (if (file-is-directory? root-dir)
      (unless (file-exists? linkpath)
        (begin (display (format #f "\n+d: ~a" linkpath))
               (unless pretend (mkdir linkpath))))
      (unless (file-exists? linkpath)
        (begin
          (display (format #f "\n+l: ~a" linkpath))
          (unless pretend (symlink root-dir linkpath))))))

;; append DESTINE to LINKPATH.
(define (linkpath-destine-dir destine-dir linkpath)
  (if (string-null? destine-dir)
      linkpath
      (string-append destine-dir "/" linkpath)))

;; ========================================== CLI PARSING

(define (cli-usage-banner)
  (display "usage: Dut [-h] [-p] [-c] [-r ROOT] [-d DEST] [-i] [-v]

Yet another simple, and opinionated, dotfiles manager.

options:
  -h, --help              show this help message and exit
  -p, --pretend           simulate the linking process
  -c, --create            create dotfiles links
  -d, --dest DEST         directory destine to release links
  -r, --root ROOT         root directory with dotfiles
  -w, --wipe              remove links created in destine directory
  -o, --overwrite         overwrite existent links
  -i, --info              provide additional information
  -v, --version           display version"))

(define cli-option-list '((version     (single-char #\v) (value #f))
                          (create    (single-char #\c) (value #f))
                          (wipe      (single-char #\w) (value #f))
                          (pretend   (single-char #\p) (value #f))
                          (overwrite (single-char #\o) (value #f))
                          (info      (single-char #\i) (value #f))
                          (dest      (single-char #\d) (value optional))
                          (root      (single-char #\r) (value #t))
                          (help      (single-char #\h) (value #f))))

(define (cli-parser args)
  (let* ([option-spec cli-option-list]
         [options (getopt-long args option-spec)])
    (cli-option-run options)))

(define (cli-option-run options)
  (let ([option-wanted (lambda (option) (option-ref options option #f))])
    (cond [(option-wanted 'version)   (display version)]
          [(option-wanted 'create)    (create options)]
          [(option-wanted 'wipe)      (wipe options)]
          [(option-wanted 'pretend)   (pretend options)]
          [(option-wanted 'overwrite) (overwrite options)]
          [(option-wanted 'info)      (info options)]
          [(option-wanted 'help)      (cli-usage-banner)]
          [else                       (cli-usage-banner)])))


;; ============================================= MAIN

(define (main args)
  (cli-parser args))

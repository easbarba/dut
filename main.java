//usr/bin/env jbang "$0" "$@" ; exit $?
//DEPS info.picocli:picocli:4.7.4

// Dut -Yet another simple and opinionated dot files manager.
// Copyright (C) 2024 EAS Barbosa

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// TIP: curl -Ls https://sh.jbang.dev | bash -s - app setup

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(
    name = "Dot", mixinStandardHelpOptions = true, version = "Dot 0.1",
    description =
        "create symbolic links of a folder mirroring its tree structure into $HOME or custom folder")
class Main implements Callable<Integer> {
  @Option(names = {"-d", "--dest"}, paramLabel = "FOLDER",
          description = "destination folder to deliver links.")
  Path destination;

  @Option(names = {"-r", "--root"}, paramLabel = "FOLDER",
          description = "target folder with dotfiles.", required = true)
  Path root;

  @Option(names = {"-c", "--create"}, description = "create links of dotfiles.")
  private boolean create;

  @Option(names = {"-", "--wipe"},
          description = "remove links linked in target.")
  private boolean wipe;

  @Option(names = {"-p", "--pretend"},
          description = "demonstrate files linking.")
  private boolean pretend;

  @Option(names = {"-o", "--overwrite"},
          description = "overwrite existent links.")
  private boolean overwrite;

  @Option(names = {"-i", "--info"}, description = "provide more information.")
  private boolean information;

  public static void main(final String... args) {
    final int exitCode = new CommandLine(new Main()).execute(args);
    System.exit(exitCode);
  }

  String infoList() {
    final var result = String.format(
        "  root: %s \n  destination: %s \n  overwrite: %s, pretend: %s, create: %s, wipe: %s\n",
        root, destination, overwrite, pretend, create, wipe);

    return result;
  }

  @Override
  public Integer call() throws Exception {
    if (information) {
      System.out.println(infoList());
    }

    apply(root, destination, create, wipe, pretend, overwrite);

    return 0;
  }

  void apply(final Path root, final Path target, final boolean create,
             final boolean wipe, final boolean pretend,
             final boolean overwrite) {
    final var actions = new Actions(root, target);
    final var ignored = new Ignored(root);
    final var homeDir = System.getProperty("user.home");

    try {
      Files.walk(root).filter(path -> !ignored.toIgnore(path)).forEach(link -> {
        if (create) {
          actions.create(link);
        }

        if (wipe) {
          actions.wipe(link);
        }
      });
    } catch (final IOException e) {
      System.out.println(e);
    }
  }
}

class Actions {
  private final Helpers helpers = new Helpers();
  private final Path root;
  private Path target;

  public Actions(final Path root, final Path target) {
    this.target = target.normalize();
    this.root = root.normalize();
  }

  private final Path toTarget(final Path link) {
    return Path.of(
              link.toString().replace(root.toString()+"/", target.toString() + "/"));
  }

  public void create(final Path filepath) {
    final var link = toTarget(filepath);

    // helpers.backup_item(filepath);
    helpers.make_folder(filepath, link);
    helpers.link_file(filepath, link);
    // helpers.fix_permission(filepath);
  }

  public void wipe(final Path link) { helpers.remove_faulty_link(link); }

  public void overwrite(final Path link) {
    helpers.link_file(link, link); // target
    helpers.fix_permission(link);
  }

  public void pretend(final Path link) {
    System.out.println(link + "->" + "homey");
  }
}

class Ignored {
  private final Path root;
  private final String[] defaultOnes = {".git", ".dutignore"};

  public Ignored(final Path root) { this.root = root; }

  // Shoudl the current filepath to be ignored?
  public Boolean toIgnore(final Path filepath) {
    return all().stream().anyMatch(
        it
        -> filepath.toString().replace("/da/personal/lar/", "").startsWith(it));
  }

  public List<String> all() {
    final List<String> result = new ArrayList<String>();
    result.addAll(Arrays.asList(defaultOnes));
    result.addAll(ignoredOnes());

    return result;
  }

  private List<String> ignoredOnes() {
    final var dotsFile = Paths.get(this.root.toString(), ".dutignore");
    List<String> dots = null;

    try {
      final Stream<String> listedDots = Files.lines(dotsFile);
      dots = listedDots.distinct()
                 .sorted(Comparator.reverseOrder())
                 .collect(Collectors.toList());

      listedDots.close();
    } catch (final IOException e) {
      System.out.println("Caught " + e);
    }

    return dots;
  }
}

class Helpers {
  public void make_folder(final Path filepath, final Path link) {
    if(Files.isDirectory(filepath))  {
      if (Files.exists(link)) {
        // System.out.println("directory already exists: " + link);
        return;
      }

      try {
        System.out.println("directory: " + link);
        Files.createDirectories(link);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void backup_item(final Path link) {
    throw new UnsupportedOperationException("not implemented");
  }

  public void remove_faulty_link(final Path link) {
    throw new UnsupportedOperationException("not implemented");
  }

  public void link_file(final Path filepath, final Path link) {
    if(Files.isRegularFile(filepath))  {
      if (Files.exists(link)) {
        // System.out.println("file already exists: " + link);
        return;
      }

      try {
        System.out.println("link: " + link);
        Files.createSymbolicLink(link, filepath);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void fix_permission(final Path link) {
    throw new UnsupportedOperationException("not implemented");
  }
}

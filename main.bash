#!/usr/bin/env bash

# Dut -Yet another simple and opinionated dot files manager.
# Copyright (C) 2024 EAS Barbosa

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Debug Options
set -euo pipefail

# VARIABLES
ARGS=()
IGNORED=()

declare -A VALUES
VALUES=([from]='' [to]='')

declare -A ACTIONS
ACTIONS=([create]=false [remove]=false [pretend]=false [overwrite]=false [info]=false)

# CLI OPTIONS
usage() {
    cat <<EOF
Usage: dot [options]
    -t DIR,  --to DIR                destination folder to deliver links.
    -f DIR,  --from DIR              target folder with dotfiles.
    -c,      --create                create links of dotfiles.
    -r,      --remove                remove links from target folder.
    -p,      --pretend               demonstrate files linking.
    -o,      --overwrite             overwrite existent links.
EOF
    exit 0
}

# No arguments provided
[[ $# -eq 0 ]] && usage

# -- OPTIONS:

# Replace long arguments
for arg; do
    case "$arg" in
        --help) ARGS+=(-h) ;;
        --from) ARGS+=(-f) ;;
        --to) ARGS+=(-t) ;;
        --create) ARGS+=(-c) ;;
        --remove) ARGS+=(-r) ;;
        --pretend) ARGS+=(-p) ;;
        --overwrite) ARGS+=(-o) ;;
        --info) ARGS+=(-i) ;;
            #) ARGS+=("$arg") ;;
    esac
done

set -- "${ARGS[@]}"

while getopts "hcrpoif:t:" OPTION; do
    case $OPTION in
        h) usage ;;
        f) VALUES[from]="$OPTARG" ;;
        t) VALUES[to]="$OPTARG" ;;
        c) ACTIONS[create]=true ;;
        r) ACTIONS[remove]=true ;;
        p) ACTIONS[pretend]=true ;;
        o) ACTIONS[overwrite]=true ;;
        i) ACTIONS[info]=true ;;
        *) usage ;;
    esac
done

# -- BUSSINESS LOGIC:

# --from is a must!
[[ -z ${VALUES[from]} ]] && echo "Missing required option: '--from DIR'" && exit

# LIST OF FILES TO BE IGNORED
IGNORED_FILE="${VALUES[from]}/.dutignore"
[[ -f $IGNORED_FILE ]] && mapfile -t IGNORED <"$IGNORED_FILE"
IGNORED+=(.git) # user should set it, but lets be safe!

print_info() {
    echo "    -- Values
    "

    for v in "${!VALUES[@]}"; do
        echo "$v: ${VALUES[$v]}"
    done

    echo "
    -- Actions
    "

    for v in "${!ACTIONS[@]}"; do
        echo "$v: ${ACTIONS[$v]}"
    done

    echo "
    -- Internals
    "
    echo "ignored: ${IGNORED[@]}"

    exit 1
}

# ACTIONS
[[ ${ACTIONS[info]} ]] && print_info

exit

# ON-GOING
for f in $(find "$FROM"); do
    FILE="${f#${FROM}/}"

    IGNORED_FOUND=$(echo "${IGNORED[@]}" | grep -o "$FILE" | wc -w)
    [[ ! $IGNORED_FOUND -gt 0 ]] && continue # echo "ignoring: $FILE"
    echo "$FILE"
    # [[ $FILE =~ ^.git/ ]] && continue
done

exit

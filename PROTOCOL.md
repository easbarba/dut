<!--
 Gota is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Gota is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Gota. If not, see <https://www.gnu.org/licenses/>.
-->

# Protocol

## Core

- Exactly mirrors target's folder structure.
- Folders are created not linked, to avoid general issues.

## .dutignore

- If a `.dutignore` is present, all files listed are to be ignored, eg: `LICENSE`.
- `.git` is, of course, ignored by default.

## Symbols

| symbol | description       |
|--------|-------------------|
| l      | new link          |
| d      | directory created |
| b      | backup done       |
| r      | link removed      |

## Remove

- remove all linked files, but preserve folders, as there may be others files.

## Overwrite

- overwrite present links
- optional

## Pretend

- Pretend mode
- optional

## Common

- Remove faulty symbolic links found.
- Back up non-symbolic link files to `$HOME/.backup` folder.

## OS

- Unix-like distributions only.
